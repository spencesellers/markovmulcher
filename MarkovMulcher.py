import random
import sys
def construct_chain(fname, n, numletters = None):
    if fname:
        f = open(fname)
        text = f.read()
    else:
        text = sys.stdin.read()
    chain = {}
    lwords = ['']*n
    for i in range(n):
        lwords[i] = ''
    text = text.replace('\n', ' ')
    text = text.replace('\t', ' ')
    text = ' '.join(text.split())
    if numletters:
        l = [text[start:start+numletters] for start in range(0, len(text), numletters)]
    else:
        l = text.split(' ')
    for word in l:
        chain.setdefault(tuple(lwords[:-1]), []).append(lwords[-1])
        lwords.pop(0)
        lwords.append(word)
    chain.setdefault(tuple(lwords[:-1]), [].append(''))
    return chain
def show_help():
    print "-f <file>: Read from file."
    print "-s <int>: Uses letter mode, and specifies the size."
    print "-l <int>: Length of output, in chain units. Defaults to 1000."
    print "-n <int>: 'Order' of the markov chain."
    print "-h: Prints this help and quits."
def main():
    print "Working..."
    numletters = None
    n = 3
    filename = ''
    storylength = 1000
    for i in range(len(sys.argv)):
        if sys.argv[i] == '-w':
            numletters = None
        elif sys.argv[i] == '-h':
            show_help()
            return
        elif sys.argv[i] == '-s':
            numletters = int(sys.argv[i + 1])
        elif sys.argv[i] == '-n':
            n = int(sys.argv[i + 1]) + 1
        elif sys.argv[i] == '-f':
            filename = sys.argv[i + 1]
        elif sys.argv[i] == '-l':
            storylength = int(sys.argv[i + 1])
    chain = construct_chain(filename, n, numletters)
    first = random.choice(chain.keys())
    words = list(first)
    story = []
    print "\n\n\n" + '='*10
    for i in range(1, storylength):
        try:
            new_word = random.choice(chain[tuple(words)])
        except KeyError:
            print "Known world ended."
            break
        except TypeError:
            print "Reached end of file!!!"
            break
        if new_word == '':
            print "==Empty String=="
        words.pop(0)
        words.append(new_word)
        story.append(new_word)
        if story[-1].endswith('.') and random.randint(1, 5) == 1:
            story.append('\n\n')
        if not numletters:
            story.append(' ')
    print(''.join(story))
main()
